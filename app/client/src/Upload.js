import React, {Component} from 'react'

class Upload extends Component {
	constructor(props) {
		super(props)
		this.fileInput = React.createRef()
	}

	handleSubmit(event) {
		event.preventDefault();
		//will change to a fetch/axios call
		alert(
			`Selected file - ${this.fileInput.current.files[0].name}`
		)
	}

	render() {
		return (
			<form onSubmit={this.handleSubmit}>
				<label>Upload blender file:</label>
				<input type="file" ref={this.fileInput} />
				<br />
				<button type="submit">Submit</button>
			</form>
		);
	}
}

export default Upload
