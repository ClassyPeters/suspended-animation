import React, {Component} from 'react'
import {authHeaders} from './authHelper'

class FileOption extends Component { 
    constructor(props) {
	super(props);
	this.files = [];
    }

    async getFiles() {
	const response = await fetch("/files/", { headers: authHeaders() }) 
	this.files =  await response.json()
    }

    buildOptions() {
	const files = this.files
	const fileOptions = files.map(file => <option key={file.id} id={file.id}>{file.name}</option>);
	console.log(fileOptions[0])
	return {fileOptions}
    }

    render() {
	const fileOptions = this.buildOptions()
	return (
	    <div>
	    {fileOptions}
	    </div>
	) 
    }
}

export default FileOption
