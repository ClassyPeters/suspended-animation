import React, {Component} from 'react'
import LoginForm from './LoginForm'
import JobForm from './JobForm'
import handleChange from './handleChange'
import {getToken} from './authHelper'

class App extends Component {
    constructor(props) {
	super(props);
	this.state = {isLoggedIn: false};
    }

    handleChange = handleChange.bind(this);

    updateLoggedIn = () => {
	let isLoggedIn = false
	if(getToken()) {
	    isLoggedIn = true
	}
	this.setState({isLoggedIn})
	
    }
    
    render() {
	    const isLoggedIn = this.state.isLoggedIn;
	    let element;
	    if (isLoggedIn) {
		element = <JobForm />
	    } else {
		element = <LoginForm onLoggedIn={this.updateLoggedIn}/>
	    }

	return (
	    <div className="container">
		<a href="/docs">Docs</a>
		{element}
	    </div>
	)}
}

export default App
