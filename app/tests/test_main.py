"""
Complete e2e test that starts the server, and goes through a complete job cycle.
From user creation, blender upload, job creation, node creation, task scheduling
"""
import json
import os
import shutil

from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from ..utils.database import get_db
from ..main import app
from ..utils.files import get_upload_folder
from ..utils.models import Base

os.environ["SQLALCHEMY_DATABASE_URL"] = "sqlite:///./test.db"

### begin sql testing magic ###

engine = create_engine("sqlite:///./test.db", connect_args={"check_same_thread": False})
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

def override_get_db():
    """overides the session creator elsewhere in code"""
    try:
        # pylint: disable=invalid-name
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()

def override_get_upload_folder():
    # pylint: disable=missing-function-docstring
    return "app/tests/files/"

def base_directory_print():
    # pylint: disable=missing-function-docstring
    print("Did you run the tests from the base directory above app?")

app.dependency_overrides[get_db] = override_get_db
app.dependency_overrides[get_upload_folder] = override_get_upload_folder

# make sure previous databases go away
if os.path.isfile("test.db"):
    os.remove("test.db")
else:
    pass

Base.metadata.create_all(bind=engine)

### end sql testing magic ###

### begin file testing clean up ###
if os.path.exists("app/tests/files"):
    shutil.rmtree("app/tests/files/")
os.mkdir("app/tests/files/")
### end file testing clean up ###

# create a testing instance of the app that we can connect to
client = TestClient(app)


# later filled in during the remaining tests
TOKEN = None
HEADERS = None
NODE_TOKEN = None
SECOND_NODE_TOKEN = None
NODE_HEADERS = None
SECOND_NODE_TOKEN = None
SECOND_NODE_HEADERS = None


def test_server_response():
    """this ensures that the test server started correctly (imports)
    and is responding to requests"""
    response = client.get("/status")
    assert response.status_code == 200


def test_create_user():
    """tests that the server allows users to be created in an empty
    database and that the checks in place for that work"""
    payload = {
        "username": "test",
        "email": "test@example.com",
        "admin": True,
        "password": "test",
    }
    response = client.post("/auth/create", data=json.dumps(payload))
    assert response.status_code == 200


def test_login():
    # pylint: disable=global-statement
    """after a user is created, tests that logging in and obtaining
    a token works"""
    payload = {"username": "test", "password": "test"}
    response = client.post("/auth/token", data=payload)
    response_json = json.loads(str(response.text))
    global TOKEN
    assert TOKEN is None
    print("TOKEN should be None: " + str(TOKEN))
    TOKEN = response_json["access_token"]
    print("Should have something now: " + TOKEN)
    print(response)
    # base64 encoded beginning of every jwt
    assert "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9" in TOKEN
    assert response.status_code == 200


def test_token():
    # pylint: disable=global-statement
    """submits the OAuth TOKEN to make sure it's valid"""
    global HEADERS
    HEADERS = {"Authorization": "Bearer " + TOKEN}
    response = client.get("/files/", headers=HEADERS)
    print(response)
    assert response.status_code == 200


def test_create_node():
    # pylint: disable=global-statement
    """tests that nodes can be created"""
    global NODE_TOKEN
    global NODE_HEADERS
    payload = {
        "name": "test_node",
        "status": "created",
        "render": "true",
        "ffmpeg": "true",
    }
    response = client.post("/node/", headers=HEADERS, data=json.dumps(payload))
    print(response.text)
    response_json = json.loads(str(response.text))
    NODE_TOKEN = response_json[1]
    NODE_HEADERS = {"Authorization": "Bearer " + NODE_TOKEN}
    assert response.status_code == 200
    # base64 encoded beginning of every jwt
    assert "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9" in NODE_TOKEN


def test_create_second_node():
    # pylint: disable=global-statement
    """some race conditions will only show up if multiple nodes are requesting tasks"""
    global SECOND_NODE_TOKEN
    global SECOND_NODE_HEADERS
    payload = {
        "name": "second_test_node",
        "status": "created",
        "render": "true",
        "ffmpeg": "true",
    }
    response = client.post("/node/", headers=HEADERS, data=json.dumps(payload))
    response_json = json.loads(str(response.text))
    SECOND_NODE_TOKEN = response_json[1]
    SECOND_NODE_HEADERS = {"Authorization": "Bearer " + SECOND_NODE_TOKEN}
    assert response.status_code == 200
    # base64 encoded beginning of every jwt
    assert "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9" in SECOND_NODE_TOKEN

def test_file_upload():
    """tests that files can be uploaded"""
    with open("app/tests/smol.blend", "rb") as file:
        files = {"file": file}
        response = client.post("/files/", headers=HEADERS, files=files)
    print(response.text)
    base_directory_print()
    assert response.status_code == 200

def test_create_job():
    """tests that jobs can be created"""
    payload = {
        "frame_start": 1,
        "frame_end": 10,
        "frame_rate": 24,
        "samples": 0,
        "name": "test job",
        "file_id": 1,
        "output_file_type": "archive",
        "image_format": "PNG",
        "render_engine": "CYCLES",
    }
    response = client.post("/job/render", headers=HEADERS, data=json.dumps(payload))
    print(response.text)
    response_json = json.loads(str(response.text))
    assert response.status_code == 200
    assert response_json["id"] is not None


def test_tasks_created():
    """ensures that after job creation that tasks are created
    and users can check on tasks in progress"""
    response = client.get("/job/render/1/tasks/", headers=HEADERS)
    response_json = json.loads(str(response.text))
    assert response.status_code == 200
    assert len(response_json) == 10


def test_no_delete_file_while_job_active():
    """ensures that the DELETE method can't be used on a file while it's in use by a job"""
    response = client.delete("/files/1", headers=HEADERS)
    response_json = json.loads(str(response.text))
    print(response_json)
    assert response.status_code == 403


def test_node_job_query():
    """ensures that after a file is uploaded and a job created that a node
    can get the list of created jobs"""
    response = client.get("/node/job/?status=started", headers=NODE_HEADERS)
    response_json = json.loads(str(response.text))
    assert len(response_json) >= 1
    print(response_json)
    assert response_json[0]["id"] == 1
    assert response.status_code == 200


def test_node_task_request():
    """Checks that a node can request a task"""
    response = client.get("/node/task/?file=1&status=created", headers=NODE_HEADERS)
    response_json = json.loads(str(response.text))
    print(response_json)
    assert response_json["task"]["id"] == 1
    assert response.status_code == 200


def test_second_node_task_request():
    """Checks that a second node receives a different task than the first"""
    response = client.get(
        "/node/task/?file=1&status=created", headers=SECOND_NODE_HEADERS
    )
    response_json = json.loads(str(response.text))
    print(response_json)
    assert response_json["task"]["id"] == 2
    assert response.status_code == 200


def test_maintenance():
    """Checks that a node can be put into maintenance mode"""
    payload = {
        "name": "test_node",
        "status": "maintenance",
        "render": "true",
        "ffmpeg": "true",
    }
    response = client.put("/node/1", headers=HEADERS, data=json.dumps(payload))
    assert response.status_code == 200


def test_task_deallocation():
    """Checks that after a node is put into maintenance that all associated tasks
    are deallocated"""
    response = client.get("/job/render/1/tasks/?task_status=started", headers=HEADERS)
    response_json = json.loads(response.text)
    print(response_json)
    # task in this list should not be assigned to node 1
    assert response_json[0]["node"] == 2
    assert response.status_code == 200


def test_maintenance_from_node():
    """Checks that a node can't receive additional tasks in maintenance mode"""
    response = client.get("/node/task/", headers=NODE_HEADERS)
    print(json.loads(response.text))
    assert response.status_code == 400


def test_un_maintenance():
    """Checks that nodes can be taken out of maintenance mode"""
    payload = {
        "name": "test_node",
        "status": "created",
        "render": "true",
        "ffmpeg": "true",
    }
    response = client.put("/node/1", headers=HEADERS, data=json.dumps(payload))
    response_json = json.loads(response.text)
    print(response_json)
    assert response_json["status"] == "created"
    assert response.status_code == 200


def test_un_maintenance_node():
    """Checks that after being taken out of maintenance that nodes can get tasks"""
    response = client.get("/node/task/?file=1&status=created", headers=NODE_HEADERS)
    response_json = json.loads(response.text)
    print(response_json)
    assert response_json["task"]["id"] == 1
    assert response.status_code == 200


def test_no_finish_tasks_without_result():
    """Checks that tasks can't complete without a result being uploaded"""
    payload = {"status": "complete"}
    response = client.put(
        "/node/task/1", headers=NODE_HEADERS, data=json.dumps(payload)
    )
    response_json = json.loads(response.text)
    print(response_json)
    assert response.status_code == 400


def test_task_reassignment():
    """Checks that tasks can be completed normally after maintenance mode"""
    response = client.get("/node/task/?file=1&status=created", headers=NODE_HEADERS)
    print(json.loads(response.text))
    assert response.status_code == 200


def test_node_upload():
    """uses the sample rendered frames in the tests directory to see if uploads work from nodes"""
    with open(os.path.join("app", "tests", "frame1.png"), "rb") as file:
        files = {"file": file}
        response = client.post("/node/result/render/1", headers=NODE_HEADERS, files=files)
    base_directory_print()
    assert response.status_code == 200


def test_node_task_complete():
    """Tests that a node can mark a task as complete"""
    payload = {"status": "complete"}
    response = client.put(
        "/node/task/1", headers=NODE_HEADERS, data=json.dumps(payload)
    )
    response_json = json.loads(response.text)
    print(response_json)
    assert response.status_code == 200


def test_second_node_upload():
    """uses sample frame to test that a second node can upload"""
    with open(os.path.join("app", "tests", "frame2.png"), "rb") as file:
        files = {"file": file}
        response = client.post(
            "/node/result/render/2", headers=SECOND_NODE_HEADERS, files=files
        )
    base_directory_print()
    assert response.status_code == 200

def test_second_node_task_complete():
    """tests that the second node can complete a task"""
    payload = {"status": "complete"}
    response = client.put(
        "/node/task/2", headers=SECOND_NODE_HEADERS, data=json.dumps(payload)
    )
    print(response.text)
    assert response.status_code == 200


def test_job_size_change():
    """tests that a job's size can be changed"""
    payload = {"frame_start": 3, "frame_end": 5}
    response = client.put("/job/render/1", headers=HEADERS, data=json.dumps(payload))
    response_json = json.loads(response.text)
    print(response.text)
    assert response.status_code == 200
    assert response_json["frame_start"] == 3
    assert response_json["frame_end"] == 5


def test_job_pause():
    """tests that a job can be paused"""
    payload = {"status": "paused"}
    response = client.put("/job/render/1", headers=HEADERS, data=json.dumps(payload))
    response_json = json.loads(response.text)
    print(response.text)
    assert response.status_code == 200
    assert response_json["status"] == "paused"


def test_tasks_paused():
    """tests that tasks are paused"""
    response = client.get("/job/render/1/tasks/?skip=1&limit=100", headers=HEADERS)
    response_json = json.loads(response.text)
    print(response.text)
    assert response.status_code == 200
    for task in response_json:
        assert task["status"] != "created"


def test_node_job_pause():
    """tests that a node can't pick up tasks for a paused job"""
    response = client.get("/node/task/?file=1&status=created", headers=NODE_HEADERS)
    print(response.text)
    assert response.status_code == 404


def test_job_resume():
    """tests that a job can be resumed"""
    payload = {"status": "started"}
    response = client.put("/job/render/1", headers=HEADERS, data=json.dumps(payload))
    response_json = json.loads(response.text)
    print(response.text)
    assert response.status_code == 200
    assert response_json["status"] == "started"


def test_finish_remaining_tasks():
    """Tests that a node can get the remaining tasks and complete them"""
    for _ in range(3, 6):
        task_response = client.get(
            "/node/task/?file=1&status=created", headers=NODE_HEADERS
        )
        task = json.loads(task_response.text)
        print(task_response.text)
        assert task["task"]["id"] == _
        assert task_response.status_code == 200
        with open(os.path.join("app", "tests", "frame2.png"), "rb") as file:
            files = {"file": file}
            upload_resource = "/node/result/render/" + str(task["task"]["id"])
            upload_response = client.post(
                upload_resource, headers=NODE_HEADERS, files=files
            )
        base_directory_print()
        print(upload_response.text)
        assert upload_response.status_code == 200
        payload = {"status": "complete"}
        update_resource = "/node/task/" + str(_)
        update_response = client.put(
            update_resource, headers=NODE_HEADERS, data=json.dumps(payload)
        )
        print(update_response.text)
        assert update_response.status_code == 200


def test_job_status_finished():
    """Makes sure that a jobs status changes to finished after tasks complete"""
    response = client.get("/job/render/1", headers=HEADERS)
    response_json = json.loads(response.text)
    print(response_json)
    assert response_json["status"] in {"complete","archiving"}
    assert response.status_code == 200


def test_archive_download():
    """tests that a result can be downloaded"""
    job_response = client.get("/job/render/1", headers=HEADERS)
    print(job_response.text)
    response = client.get("/job/render/1/result", headers=HEADERS)
    assert response.status_code == 200


def test_file_delete():
    """tests that a file can be deleted after a job is finished"""
    response = client.delete("/files/1", headers=HEADERS)
    response_json = json.loads(response.text)
    print(response_json)
    assert response.status_code == 200
    assert response_json["deleted"]


def test_job_result_delete():
    """tests that a job's results can be deleted after completion"""
    response = client.delete("/job/render/1/result/", headers=HEADERS)
    response_json = json.loads(response.text)
    print(response_json)
    assert response.status_code == 200
    print(os.listdir(path="app/tests/files/1/1/"))
    assert len(os.listdir(path="app/tests/files/1/1/")) == 0
