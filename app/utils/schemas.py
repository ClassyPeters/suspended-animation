"""Models used for API interaction.
Used for everything from reducing input fields to sanitizing database objects"""
from datetime import datetime
from typing import Optional

from pydantic import BaseModel


class UserBase(BaseModel):
    """as with all Base schemas, this defines the request body for creating
    an object of this sort"""

    username: str
    email: str
    admin: bool


class UserCreate(UserBase):
    """keeps everyting from UserBase, and includes a password field
    so that not all request or response bodies would need to include it"""

    password: str


class User(UserBase):
    """everything in a user except the password. Used for request/response
    bodies after user creation"""

    id: int

    class Config:
        orm_mode = True


class FileCreate(BaseModel):
    name: str
    size: int
    # in bytes
    user: int


class File(FileCreate):
    """request/response body used after file creation"""

    id: int
    uploaded_at: datetime
    deleted: bool = None
    deleted_at: datetime = None

    class Config:
        orm_mode = True


class Result(BaseModel):
    """The result of a Task's execution"""

    id: int
    job: int
    name: str
    deleted: bool
    deleted_at: datetime = None

    class Config:
        orm_mode = True


class JobBase(BaseModel):
    """request body schema for job creation"""

    frame_start: int
    frame_end: int
    frame_rate: float = 24.0
    samples: int
    file_id: int
    name: str
    image_format: str = "PNG"
    output_file_type: str = "archive"
    render_engine: str = "CYCLES"


class Job(JobBase):
    """includes everything from JobBase, and adds the id for request/responses"""

    start_time: datetime
    end_time: datetime = None
    status: str
    id: int

    class Config:
        orm_mode = True


class JobUpdate(BaseModel):
    """body used for job updates from a user"""

    status: Optional[str] = None
    frame_start: Optional[int] = None
    frame_end: Optional[int] = None


class TaskBase(BaseModel):
    """base schema shared between render and encoding tasks"""

    job: int
    node: int
    status: str
    start_time: datetime
    end_time: datetime


class RenderTaskBase(TaskBase):
    """schema for information needed to create a task object"""

    frame: int
    image_format: str


class RenderTask(RenderTaskBase):
    """task schema used after database creation"""

    id: int

    class Config:
        orm_mode = True


class EncodeTaskBase(TaskBase):
    """schema for creating an encoding task object"""

    frame_start: int
    frame_end: int
    frame_rate: float
    user: int
    job: int


class EncodeTask(EncodeTaskBase):
    id: int

    class Config:
        orm_mode = True


class TaskUpdate(BaseModel):
    status: str


class TaskLogBase(BaseModel):
    log: str


class RenderTaskLog(TaskLogBase):
    id: int
    node: int
    submit_time: str


class EncodeTaskLog(TaskLogBase):
    id: int
    node: int
    submit_time: str


class NodeCreate(BaseModel):
    name: str
    status: str = "created"
    render: bool = True
    ffmpeg: bool = False


class Node(NodeCreate):
    id: int
    expiration: datetime

    class Config:
        orm_mode = True
