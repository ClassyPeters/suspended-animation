"""Utility functions related to authentication,
such as signing tokens and salting+hashing passwords"""

import os
from datetime import datetime, timedelta

# third party
import jwt
from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer
from passlib.context import CryptContext

# pylint: disable=no-name-in-module
from pydantic import BaseModel
from sqlalchemy.orm import Session
from starlette.status import HTTP_401_UNAUTHORIZED

# Settings needs to be imported here for the secret key to be loaded correctly
# pylint: disable=unused-import
from app.utils import crud, settings

SECRET_KEY = os.getenv("SECRET_KEY")

ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 50000

password_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/auth/token")


# pylint: disable=too-few-public-methods
class Token(BaseModel):
    """Generic class for all tokens"""

    access_token: str
    token_type: str


def get_password_hash(password):
    # pylint: disable=missing-function-docstring
    return password_context.hash(password)


def verify_password_with_hash(password, hashed_password,):
    # pylint: disable=missing-function-docstring
    return password_context.verify(password, hashed_password)


def authenticate_user_password(db: Session, username: str, password: str,):
    """Verify that a user's supplied password is the same as the hashed version"""
    # pylint: disable=invalid-name
    user = crud.get_user_by_username(db, username)
    if not user:
        return False
    if not verify_password_with_hash(password, user.password):
        return False
    return user


def create_access_token(*, data: dict, expires_delta: timedelta = None,):
    """Creates a signed access token"""
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=30)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


def get_current_client(
    # pylint: disable=invalid-name
    db: Session,
    token_type: str,
    token: str = Depends(oauth2_scheme),
):
    """Returns the current client making the request, after verifying
    that a client exists and has a signed token"""
    credentials_exception = HTTPException(
        status_code=HTTP_401_UNAUTHORIZED,
        detail="could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    # pylint: disable=raise-missing-from
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        assert token_type == payload.get("token_type")
        if token_type == "user":
            username = payload.get("sub")
            assert username is not None
            client = crud.get_user_by_username(db, username=username)
            assert client is not None
        elif token_type == "node":
            name = payload.get("sub")
            assert name is not None
            client = crud.get_node_by_name(db, name=name)
            assert client is not None
    except Exception:
        raise credentials_exception
    return client
