"""Miscelaneous utility functions"""
from fastapi import HTTPException

def check_for_404(query_result):
    """Shorthand function for returning a 404 error is the database returns nothing"""
    if query_result is None:
        raise HTTPException(status_code=404, detail="Not Found")
