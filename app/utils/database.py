"""Module for loading and creating a database"""
import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# settings must be imported for environment to load correctly
# pylint: disable=unused-import
from app.utils import settings

SQLALCHEMY_DATABASE_URL = os.getenv("SQLALCHEMY_DATABASE_URL")

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

def get_db():
    """creates database sessions as neeeded, used as a dependency"""
    try:
        # pylint: disable=invalid-name
        db = SessionLocal()
        yield db
    finally:
        db.close()
