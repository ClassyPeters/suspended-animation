"""Collection of functions for interfacing with the database"""
from datetime import datetime

from sqlalchemy.orm import Session

from app.utils import models, schemas

def create_user(db: Session, user: schemas.UserCreate):
    """Creates a user in the database. The password is expected to be
    hashed before this function is called"""
    db_user = models.User(
        username=user.username,
        email=user.email,
        password=user.password,
        admin=user.admin,
        active=True,
    )
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def get_user_by_username(db: Session, username: str):
    # pylint: disable=missing-function-docstring
    return db.query(models.User).filter(models.User.username == username).first()

def get_user_by_email(db: Session, email: str):
    # pylint: disable=missing-function-docstring
    return db.query(models.User).filter(models.User.email == email).first()

def get_first_user(db: Session):
    # pylint: disable=missing-function-docstring
    return db.query(models.User).first()

def create_node(db: Session, node: schemas.NodeCreate, expiration):
    """Creates a worker node object in the database and returns it"""
    db_node = models.Node(
        name=node.name,
        status=node.status,
        expiration=expiration,
        render=node.render,
        ffmpeg=node.ffmpeg,
    )
    db.add(db_node)
    db.commit()
    db.refresh(db_node)
    return db_node

def get_node_by_id(db: Session, node_id: int):
    # pylint: disable=missing-function-docstring
    return db.query(models.Node).filter(models.Node.id == node_id).first()

def get_node_by_name(db: Session, name: str):
    # pylint: disable=missing-function-docstring
    return db.query(models.Node).filter(models.Node.name == name).first()

def get_nodes(db: Session, status: str = None, skip: int = 0, limit: int = 100):
    """Returns a list of nodes based on the given status, skip, and limit"""
    nodes = None
    if status is None:
        nodes =  db.query(models.Node).offset(skip).limit(limit).all()
    else:
        nodes = (
                    db.query(models.Node)
                    .filter(models.Node.status == status)
                    .offset(skip)
                    .limit(limit)
                    .all()
                )
    return nodes

def update_node(db: Session, node_id: int, node: schemas.NodeCreate):
    """Updates an existing worker node object"""
    db_node = db.query(models.Node).filter(models.Node.id == node_id).first()
    db_node.name = node.name
    db_node.status = node.status
    db_node.render = node.render
    db_node.ffmpeg = node.ffmpeg
    db.commit()
    db.refresh(db_node)
    return db_node

def deallocate_all_node_tasks(db: Session, node_id: int):
    """Any active or 'stuck' tasks are unassigned from the given node to be picked up by others"""
    db_tasks = (
        db.query(models.RenderTask)
        .filter(models.RenderTask.node == node_id)
        .filter(models.RenderTask.status != "complete")
        .filter(models.RenderTask.status != "canceled")
        .all()
    )
    for task in db_tasks:
        task.status = "created"
        task.start_time = None
        task.node = None
        db.add(task)
    db.commit()

def get_expired_nodes(db: Session, expires: datetime):
    """Returns all nodes that haven't called the server recently"""
    return db.query(models.Node).filter(models.Node.expiration <= expires).all()


def create_file_entry(db: Session, file: schemas.FileCreate):
    """Creates a blender file entry in the database"""
    db_file = models.File(
        name=file.name,
        size=file.size,
        user=file.user,
        uploaded_at=datetime.utcnow(),
    )
    db.add(db_file)
    db.commit()
    db.refresh(db_file)
    return db_file


def get_file_entry(db: Session, file_id: int):
    # pylint: disable=missing-function-docstring
    return db.query(models.File).filter(models.File.id == file_id).first()

def get_files_by_user(db: Session, user: int, skip: int = 0, limit: int = 100):
    """Returns a list of files for a specific user with a given skip and limit"""
    return (
        db.query(models.File)
        .filter(models.File.user == user)
        .offset(skip)
        .limit(limit)
        .all()
    )

def create_result(db: Session, job: int, task: int, name: str):
    """Creates a database object for the result of a render task"""
    db_result = models.Result(job=job, task=task, name=name)
    db.add(db_result)
    db.commit()
    db.refresh(db_result)
    return db_result

def get_result_by_task(db: Session, task_id: int):
    """Returns the completed result for a given task"""
    return db.query(models.Result).filter(models.Result.task == task_id).first()

def get_results_by_job(db: Session, job: int, skip: int = 0, limit: int = 100):
    """Returns all finished task results for a given job, skip, and limit"""
    return (
        db.query(models.Result)
        .filter(models.Result.job == job)
        .offset(skip)
        .limit(limit)
        .all()
    )

def get_job_by_id(db: Session, job_id: int):
    # pylint: disable=missing-function-docstring
    return db.query(models.Job).filter(models.Job.id == job_id).first()

def create_job(db: Session, job: schemas.JobBase, user_id: int, status: str):
    """Creates a job object in the database"""
    db_job = models.Job(
        frame_start=job.frame_start,
        frame_end=job.frame_end,
        frame_rate=job.frame_rate,
        samples=job.samples,
        file_id=job.file_id,
        name=job.name,
        image_format=job.image_format,
        output_file_type=job.output_file_type,
        status=status,
        user=user_id,
        file=job.file_id,
        start_time=datetime.utcnow(),
        end_time=None,
        render_engine=job.render_engine,
    )
    db.add(db_job)
    db.commit()
    db.refresh(db_job)
    return db_job

def increase_job_start(db: Session, db_job: models.Job, job: schemas.JobUpdate):
    """Increases the number of frames at the beginning of a job"""
    for frame in range(job.frame_start, db_job.frame_start):
        db_task = models.RenderTask(
            job=db_job.id,
            frame=frame,
            image_format=db_job.image_format,
            file=db_job.file,
            node=None,
            status="created",
        )
        db.add(db_task)
        db.commit()

def decrease_job_start(db: Session, job_id: int, job: schemas.JobUpdate):
    """Decreases how many frames are in a job's beginning"""
    db_tasks = (
        db.query(models.RenderTask)
        .filter(models.RenderTask.frame < job.frame_start, models.RenderTask.job == job_id)
        .all()
    )
    for task in db_tasks:
        task.status = "canceled"
        task.node = None
        db.commit()


def increase_job_end(db: Session, db_job: models.Job, job: schemas.JobUpdate):
    """Increases the number of frames at the end of a job"""
    for frame in range(db_job.frame_end, job.frame_end + 1):
        db_task = models.RenderTask(
            job=db_job.id,
            frame=frame,
            image_format=db_job.image_format,
            file=db_job.file,
            node=None,
            status="created",
        )
        db.add(db_task)
        db.commit()


def decrease_job_end(db: Session, job_id: int, job: schemas.JobUpdate):
    """Decreases the number of frames at the end of a job"""
    db_tasks = db.query(models.RenderTask).filter(
        models.RenderTask.frame > job.frame_end, models.RenderTask.job == job_id
    )
    for task in db_tasks:
        task.status = "canceled"
        task.node = None
        db.commit()


def update_job_status(db: Session, job_id: int, status: str):
    """Updates the status of an existing job object"""
    db_job = db.query(models.Job).filter(models.Job.id == job_id).first()
    db_job.status = status
    db.add(db_job)
    db.commit()


def get_jobs(db: Session, status: str = None, skip: int = 0, limit=100):
    """this function doesn't filter by users, (READ: insecure for users).
    It's only to be used in /node routes"""
    jobs = None
    if status is None:
        jobs =  db.query(models.Job).offset(skip).limit(limit).all()
    else:
        jobs =  (
            db.query(models.Job)
            .filter(models.Job.status == status)
            .offset(skip)
            .limit(limit)
            .all()
        )
    return jobs

def get_jobs_by_user(
    db: Session, user_id: int, status: str = None, skip: int = 0, limit=100
):
    """Returns a list of jobs for a user with a given status, skip, and limit"""
    jobs = None
    if status is None:
        jobs = (
            db.query(models.Job)
            .filter(models.Job.user == user_id)
            .offset(skip)
            .limit(limit)
            .all()
        )
    else:
        jobs = (
            db.query(models.Job)
            .filter(models.Job.user == user_id, models.Job.status == status)
            .offset(skip)
            .limit(limit)
            .all()
        )
    return jobs

def get_active_jobs_by_file(db: Session, file_id: int):
    """Returns a list of jobs using a specific file that are in progress"""
    return (
        db.query(models.Job)
        .filter(models.Job.file == file_id, models.Job.status == "started")
        .all()
    )

def get_render_task_by_id(db: Session, task_id: int):
    # pylint: disable=missing-function-docstring
    return db.query(models.RenderTask).filter(models.RenderTask.id == task_id).first()

def create_render_tasks(db: Session, job: schemas.Job):
    """Creates render tasks based for a job and leaves node assignment blank"""
    # range() is non-inclusive
    for frame in range(job.frame_start, job.frame_end + 1):
        db_task = models.RenderTask(
            job=job.id,
            frame=frame,
            file=job.file,
            node=None,
            image_format=job.image_format,
            status="created",
            render_engine=job.render_engine,
        )
        db.add(db_task)
        db.commit()

def cancel_render_tasks(db: Session, job_id: int):
    """This function is called after a job is canceled
        to prevent any remaining tasks from being run.
    It is only used after said job has been canceled"""
    db_tasks = (
        db.query(models.RenderTask)
        .filter(models.RenderTask.job == job_id, models.RenderTask.status != "complete")
        .all()
    )
    for task in db_tasks:
        task.status = "canceled"
        task.node = None
        db.add(task)
        db.commit()
    return "successfully canceled tasks"


def pause_render_tasks(db: Session, job_id: int):
    """Called when a job needs to be paused and prevents tasks from being run"""
    db_tasks = (
        db.query(models.RenderTask)
        .filter(
            models.RenderTask.job == job_id,
            models.RenderTask.status != "complete",
            models.RenderTask.status != "canceled",
        )
        .all()
    )
    for task in db_tasks:
        task.status = "paused"
        task.node = None
        db.add(task)
        db.commit()
    return "successfully paused tasks"


def resume_render_tasks(db: Session, job_id: int):
    """Called when a job needs to be resumed and allows tasks to be run again"""
    db_tasks = (
        db.query(models.RenderTask)
        .filter(models.RenderTask.job == job_id, models.RenderTask.status == "paused")
        .all()
    )
    for task in db_tasks:
        task.status = "created"
        task.node = None
        db.add(task)
        db.commit()
    return "successfully resumed tasks"

def get_first_incomplete_task_by_job(db: Session, job: int):
    """Returns the first incomplete task for a given job.
    Used for checking job completeness as tasks can be completed out of order"""
    return (
        db.query(models.RenderTask)
        .filter(models.RenderTask.job == job)
        .filter(
            models.RenderTask.status != "complete",
            models.RenderTask.status != "canceled",
        )
        .first()
    )

def get_oldest_incomplete_task_by_files(db: Session, file: list, status: str):
    """Returns the oldest incomplete task for a list of files. Used to assign tasks to workers"""
    return (
        db.query(models.RenderTask)
        .filter(models.Job.file_id.in_(file), models.RenderTask.status == status)
        .join(models.File, models.RenderTask.job == models.Job.id)
        .first()
    )

def get_tasks_by_job_id(
    db: Session, job_id: int, status: str = None, skip: int = 0, limit: int = 100
):
    """Returns a list of tasks for a job with a given task status, skip, and limit"""
    tasks = None
    if status is None:
        tasks =  (
            db.query(models.RenderTask)
            .filter(models.RenderTask.job == job_id)
            .offset(skip)
            .limit(limit)
            .all()
        )
    else:
        tasks =  (
            db.query(models.RenderTask)
            .filter(models.RenderTask.job == job_id)
            .filter(models.RenderTask.status == status)
            .offset(skip)
            .limit(limit)
            .all()
        )
    return tasks

def get_render_task_log(db: Session, task_id, node_id: int = None):
    """Returns logs for a specific task. One task may be executed by multiple nodes, so uniqueness
    isn't enforced on this table"""
    logs = None
    if node_id is None:
        logs = db.query(models.RenderTaskLog).filter(models.RenderTaskLog.task == task_id).all()
    else:
        logs =  (
            db.query(models.RenderTaskLog)
            .filter(models.RenderTaskLog.task == task_id)
            .filter(models.RenderTaskLog.node == node_id)
            .all()
        )
    return logs

def create_render_task_log(
    db: Session, task_id, task_log: str, node_id: int, submit_time,
):
    """Creates an entry in the database for logging a render task"""
    db_task_log = models.RenderTaskLog(
        task=task_id, node=node_id, log=task_log, submit_time=submit_time
    )
    db.add(db_task_log)
    db.commit()

def create_encode_task(db: Session, job: schemas.Job):
    """Creates an encode task. There should only be one encode task for each job"""
    db_task = models.EncodeTask(
        job=job.id,
        node=None,
        status="created",
        user=job.user,
        frame_start=job.frame_start,
        frame_end=job.frame_end,
        frame_rate=job.frame_rate,
    )
    db.add(db_task)
    db.commit()

def get_encode_task_by_id(db: Session, task_id: int):
    # pylint: disable=missing-function-docstring
    return db.query(models.EncodeTask).filter(models.EncodeTask.id == task_id).first()

def get_first_incomplete_encode_task(db: Session):
    # pylint: disable=missing-function-docstring
    return db.query(models.EncodeTask).filter(models.EncodeTask.status == "created").first()

def create_encode_task_log(
    db: Session, task_id: int, task_log: str, node_id: int, submit_time,
):
    """Creates a database entry for logging a render task"""
    db_task_log = models.EncodeTaskLog(
        task=task_id, node=node_id, log=task_log, submit_time=submit_time
    )
    db.add(db_task_log)
    db.commit()
