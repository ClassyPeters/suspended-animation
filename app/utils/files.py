"""Collection of utility functions for interacting with files on disk"""
import os
import shutil
import zipfile
from datetime import datetime

from sqlalchemy.orm import Session

from app.utils import crud, models, schemas


def create_user_directory(user: schemas.User, upload_folder):
    """Create a directory on disk for storing user files"""
    os.mkdir(os.path.join(upload_folder, str(user.id)))

def check_directory(user: schemas.User, upload_folder):
    """Check that a directory exists for a specific user"""
    return os.path.exists(os.path.join(upload_folder, str(user.id)))

def get_directory_string(user_id: int, filename: str, upload_folder):
    """Return a standardized directory string for other functions"""
    return os.path.join(upload_folder, user_id, filename)

def create_output_file_directory_string(job: models.Job, upload_folder):
    """Return a standardized string for final output file"""
    return os.path.join(str(upload_folder), str(job.user), str(job.id))

def get_compressed_file_directory_string(job: models.Job, upload_folder):
    """Returns a standardized string of the full file output"""
    output_file_name = create_output_file_name(job)
    output_directory_string = create_output_file_directory_string(job, upload_folder)
    return output_directory_string + "/" + output_file_name

def get_encoded_file_directory_string(job: models.Job, upload_folder):
    """Returns a standardized string of the final encoded file"""
    output_file_name = (
        str(job.id) + str(job.frame_start) + "_" + str(job.frame_end) + ".mp4"
    )
    output_directory_string = create_output_file_directory_string(job, upload_folder)
    return output_directory_string + "/" + output_file_name

def create_output_file_name(job: models.Job):
    """Creates a standardized string for final job output filenames"""
    extension = ""
    if job.output_file_type == "mp4":
        extension = ".mp4"
    elif job.output_file_type == "archive":
        extension = ".zip"
    return str(job.name) + extension

def create_job_directory(user_id: int, job_id: int, upload_folder):
    """Create a directory for a specific job"""
    os.mkdir(os.path.join(upload_folder, str(user_id), str(job_id)))

def save_file(directory: str, fileobj):
    """Save a file on disk that's submitted to the server"""
    with open(os.path.join(directory), "wb") as empty_file_obj:
        shutil.copyfileobj(fileobj, empty_file_obj)

def delete_file(upload_folder, file: models.File):
    """
    Deletes file at given location.
    Be EXTREMELY careful with this method
    """
    file_to_delete = os.path.join(
        upload_folder, str(file.user), str(file.id) + ".blend"
    )
    os.remove(file_to_delete)


def delete_job_result(upload_folder, job: models.Job):
    """Deletes the final job result, can be a zip archive or video file"""
    if job.output_file_type == "mp4":
        file_to_delete = get_encoded_file_directory_string(job, upload_folder)
        os.remove(file_to_delete)
    elif job.output_file_type == "archive":
        file_to_delete = get_compressed_file_directory_string(job, upload_folder)
        os.remove(file_to_delete)


def delete_task_results(db: Session, upload_folder, job: models.Job):
    """Delete all task results from the disk and record deletion time"""
    tasks = crud.get_tasks_by_job_id(db, job.id)
    now = datetime.utcnow()
    for task in tasks:
        file_to_delete = os.path.join(
            upload_folder,
            str(job.user),
            str(job.id),
            "frame" + str(task.frame) + "." + str(task.image_format).lower(),
        )
        if os.path.exists(file_to_delete) is False:
            continue
        os.remove(file_to_delete)
        result = crud.get_result_by_task(db, task.id)
        result.deleted = True
        result.deleted_at = now
        db.commit()

def get_file_size(directory: str):
    #pylint: disable=missing-function-docstring
    return os.path.getsize(directory)

def compress_finished_job(db: Session, job: models.Job, upload_folder):
    """This function can take a long time to run, so be careful about where it's invoked.
    It's best if this function is invoked in the background"""
    # pylint: disable=consider-using-with
    output_file_name = create_output_file_name(job)
    output_directory_string = create_output_file_directory_string(job, upload_folder)
    output_file = zipfile.ZipFile(
        output_directory_string + "/" + output_file_name,
        "w",
        compression=zipfile.ZIP_DEFLATED,
        allowZip64=True,
        compresslevel=9,
    )

    for files in os.walk(output_directory_string):
        for file in files:
            if str(job.image_format).lower() in file:
                output_file.write(
                    filename=os.path.join(
                        str(upload_folder), str(job.user), str(job.id), str(file)
                    ),
                    arcname=str(file),
                    compress_type=zipfile.ZIP_DEFLATED,
                )

    output_file.close()

    # gives visibility from the API on the status of a job
    crud.update_job_status(db, job.id, "complete")

def check_for_compressed_file(job: models.Job, upload_folder):
    """Check if the final compressed file exists, called after job completion"""
    output_file_name = create_output_file_name(job)
    output_directory_string = create_output_file_directory_string(job, upload_folder)
    return os.path.exists(output_directory_string + "/" + output_file_name)

def check_for_encoded_file(job: models.Job, upload_folder):
    """Check if the final encoded file exists, called after job completion"""
    output_file_name = (
        str(job.id) + str(job.frame_start) + "_" + str(job.frame_end) + ".mp4"
    )
    output_directory_string = create_output_file_directory_string(job, upload_folder)
    return os.path.exists(output_directory_string + "/" + output_file_name)

def get_upload_folder():
    """Returns what directory is being used for uploads, overwritten to tempfile in tests"""
    return os.path.join(os.getcwd() + "/app/files/")
