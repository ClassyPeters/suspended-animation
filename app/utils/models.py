"""All database models for the application"""
from sqlalchemy import Boolean, Column, DateTime, Float, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class User(Base):
    """table for storing user information, referenced in almost everything else"""

    __tablename__ = "user"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True)
    email = Column(String, unique=True, index=True)
    active = Column(Boolean)
    admin = Column(Boolean)
    password = Column(String)
    created_by = Column(Integer, ForeignKey("user.id"))
    created_time = Column(DateTime)


class Job(Base):
    """highest level representation of work to be done. Broken out into individual tasks"""

    __tablename__ = "job"

    id = Column(Integer, primary_key=True, index=True)
    frame_start = Column(Integer)
    frame_end = Column(Integer)
    frame_rate = Column(Float)
    samples = Column(Integer)
    file_id = Column(String)
    image_format = Column(String)  # PNG, TARGA, JPEG
    output_file_type = Column(String)  # archive or mp4
    status = Column(String)
    user = Column(Integer, ForeignKey("user.id"))
    file = Column(Integer, ForeignKey("file.id"))
    name = Column(String)
    start_time = Column(DateTime)
    end_time = Column(DateTime)
    render_engine = Column(String)

    """
    minimum task information
    id = Column(Integer, primary_key=True, index=True)
    job = Column(Integer, ForeignKey('job.id'))
    node = Column(String, ForeignKey('node.id'))
    status = Column(String)
    start_time = Column(DateTime)
    end_time = Column(DateTime)

    minimum task log information
    id = Column(Integer, primary_key=True, index=True)
    node = Column(Integer, ForeignKey('node.id'))
    log = Column(String)
    submit_time = Column(DateTime)
    """


class RenderTask(Base):
    """executable level of work for rendering, equal to one frame"""

    __tablename__ = "render_task"

    id = Column(Integer, primary_key=True, index=True)
    job = Column(Integer, ForeignKey("job.id"))
    node = Column(Integer, ForeignKey("node.id"))
    status = Column(String)
    start_time = Column(DateTime)
    end_time = Column(DateTime)
    frame = Column(Integer)
    image_format = Column(String, ForeignKey("job.image_format"))
    file = Column(String, ForeignKey("job.id"))
    render_engine = Column(String, ForeignKey("job.render_engine"))


class RenderTaskLog(Base):
    """logs submitted by a node about how a render task was executed"""

    __tablename__ = "render_task_log"

    id = Column(Integer, primary_key=True, index=True)
    node = Column(Integer, ForeignKey("node.id"))
    log = Column(String)
    submit_time = Column(DateTime)
    task = Column(Integer, ForeignKey("render_task.id"))


class EncodeTask(Base):
    """A task for nodes to understand how to execute an ffmpeg job"""

    __tablename__ = "encode_task"

    id = Column(Integer, primary_key=True, index=True)
    job = Column(Integer, ForeignKey("job.id"))
    node = Column(Integer, ForeignKey("node.id"))
    status = Column(String)
    start_time = Column(DateTime)
    end_time = Column(DateTime)
    user = Column(Integer, ForeignKey("user.id"))
    frame_start = Column(Integer, ForeignKey("job.frame_start"))
    frame_end = Column(Integer, ForeignKey("job.frame_end"))
    frame_rate = Column(Float, ForeignKey("job.frame_rate"))


class EncodeTaskLog(Base):
    """logs submitted by a node about how an encoding task was executed"""

    __tablename__ = "encode_task_log"

    id = Column(Integer, primary_key=True, index=True)
    node = Column(Integer, ForeignKey("node.id"))
    log = Column(String)
    submit_time = Column(DateTime)
    task = Column(Integer, ForeignKey("encode_task.id"))


class Node(Base):
    """a worker that executes tasks and returns a result to contribute to finishing a job"""

    __tablename__ = "node"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(Integer, index=True)
    status = Column(String)
    expiration = Column(DateTime)
    render = Column(Boolean)
    ffmpeg = Column(Boolean)


class File(Base):
    """the source file included in a job in .blend format"""

    __tablename__ = "file"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    size = Column(Integer)
    user = Column(Integer, ForeignKey("user.id"))
    uploaded_at = Column(DateTime)
    deleted = Column(Boolean)
    deleted_at = Column(DateTime)


class Result(Base):
    """the result of a finished task"""

    __tablename__ = "result"

    id = Column(Integer, primary_key=True, index=True)
    job = Column(Integer, ForeignKey("job.id"))
    task = Column(Integer, ForeignKey("render_task.id"))
    name = Column(String)
    deleted = Column(Boolean)
    deleted_at = Column(DateTime)
