"""When imported it loads the .env file in"""
from dotenv import load_dotenv

load_dotenv(override=True)
