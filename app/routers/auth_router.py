"""Routes used for authenticating with the server"""
from datetime import timedelta

from fastapi import (
    APIRouter,
    Depends,
    Header,
    HTTPException,
)
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from ..utils import auth, crud, schemas
from ..utils.database import get_db
from ..utils.other import check_for_404

router = APIRouter(prefix="/auth", tags=["Authentication"])


@router.post("/create", response_model=schemas.User)
def create_user(
    new_user: schemas.UserCreate,
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    token: str = Header(None),
):
    """Route for adding users to the server"""
    user_check = crud.get_first_user(db)
    if user_check:
        user = auth.get_current_client(db, "user", token)
        if not user.admin:
            raise HTTPException(
                status_code=401,
                detail="Insufficient Access",
            )
    new_user.username = str.lower(new_user.username)
    new_user.email = str.lower(new_user.email)
    if crud.get_user_by_username(db, new_user.username) is not None:
        raise HTTPException(status_code=400, detail="username already in use")
    if crud.get_user_by_email(db, new_user.email) is not None:
        raise HTTPException(status_code=400, detail="email already in use")
    new_user.password = auth.get_password_hash(new_user.password)
    if user_check is None:
        new_user.admin = True
    # figure out admin and user creation time with new schemas
    new_user = crud.create_user(db, new_user)
    return new_user


@router.post("/token")
def obtain_oauth_token(
    form_data: OAuth2PasswordRequestForm = Depends(),
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
):
    """OAuth token issuer"""
    user = auth.authenticate_user_password(
        db, str.lower(form_data.username), form_data.password
    )
    if not user:
        raise HTTPException(
            status_code=401,
            detail="Incorrect username or password",
            headers={"WWW-Authentice": "Bearer"},
        )
    access_token_expires = timedelta(minutes=auth.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = auth.create_access_token(
        data={"token_type": "user", "sub": user.username, "admin": user.admin},
        expires_delta=access_token_expires,
    )
    return {"access_token": access_token, "token_type": "bearer"}


@router.post("/node/{node_id}")
def get_token_for_node(
    node_id: int,
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    token: str = Depends(auth.oauth2_scheme),
):
    """This route is for administrators to obtain a new token for nodes in the farm"""
    auth.get_current_client(db, "user", token)
    node = crud.get_node_by_id(db, node_id)
    check_for_404(node)
    access_token_expires = timedelta(minutes=auth.ACCESS_TOKEN_EXPIRE_MINUTES)
    node_token = auth.create_access_token(
        data={"token_type": "node", "sub": node.name},
        expires_delta=access_token_expires,
    )
    return [node, node_token]
