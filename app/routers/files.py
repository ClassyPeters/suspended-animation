"""Routes for interacting with file objects"""
import os
from datetime import datetime

from fastapi import APIRouter, Depends, File, HTTPException, UploadFile
from sqlalchemy.orm import Session
from starlette.responses import FileResponse
from werkzeug.utils import secure_filename

from ..utils import auth, crud, files, schemas
from ..utils.database import get_db
from ..utils.files import get_upload_folder

router = APIRouter(
    prefix="/files",
    tags=["Files"],
)

@router.get("/")
def get_files(
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    token: str = Depends(auth.oauth2_scheme),
    skip: int = 0,
    limit: int = 100,
):
    """Route for a user to get a list of all the files uploaded to the server"""
    user = auth.get_current_client(db, "user", token)
    return crud.get_files_by_user(db, user.id, skip, limit)

@router.post("/", response_model=schemas.File)
async def upload_file(
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    file: UploadFile = File(...),
    token: str = Depends(auth.oauth2_scheme),
    upload_folder=Depends(get_upload_folder),
):
    """Route for users to upload blender files to the server"""
    user = auth.get_current_client(db, "user", token)
    file.filename = secure_filename(file.filename)
    try:
        os.mkdir(os.path.join(upload_folder, str(user.id)))
    except FileExistsError:
        pass
    # create database entry to get unique file ID
    temp_file_entry = schemas.FileCreate(name=file.filename, size=0, user=user.id)
    db_file = crud.create_file_entry(db, temp_file_entry)
    # save file to disk
    upload_directory = os.path.join(
        upload_folder, str(user.id), str(db_file.id) + ".blend"
    )
    files.save_file(upload_directory, file.file)
    # update database entry with disk information
    size = files.get_file_size(upload_directory)
    db_file.size = size
    db.commit()
    db.refresh(db_file)
    return db_file


@router.delete("/{file_id}", response_model=schemas.File)
async def delete_file(
    file_id: int,
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    token: str = Depends(auth.oauth2_scheme),
    upload_folder=Depends(get_upload_folder),
):
    """Route to delete a previously uploaded file"""
    user = auth.get_current_client(db, "user", token)
    db_file = crud.get_file_entry(db, file_id)
    if db_file is None or db_file.user != user.id:
        raise HTTPException(status_code=403, detail="invalid file id")
    if db_file.deleted:
        raise HTTPException(status_code=403, detail="file already deleted")
    # check that the file isn't in use by any active jobs
    jobs_using_file = crud.get_active_jobs_by_file(db, file_id)
    if len(jobs_using_file) != 0:
        jobs = []
        for _ in jobs_using_file:
            jobs.append(_.id)
        detail = (
            "File is in use on jobs: "
            + str(jobs)
            + ". Please cancel any jobs using this file to delete"
        )
        raise HTTPException(status_code=403, detail=detail)
    # delete file if previous checks pass
    files.delete_file(upload_folder, db_file)
    # update database entry
    db_file.deleted = True
    db_file.deleted_at = datetime.utcnow()
    db.commit()
    return db_file

@router.get("/{file_id}")
async def retrieve_stored_blender_file(
    file_id: int,
    token: str = Depends(auth.oauth2_scheme),
    upload_folder=Depends(get_upload_folder),
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
):
    """Allow users to download files they've uploaded"""
    user = auth.get_current_client(db, "user", token)
    db_file = crud.get_file_entry(db, file_id)
    if db_file.user != user.id or db_file is None:
        raise HTTPException(status_code=404, detail="Not Found")
    if db_file.deleted:
        raise HTTPException(status_code=403, detail="File previously deleted")
    directory = os.path.join(
        upload_folder, str(db_file.user), str(db_file.id) + ".blend"
    )
    return FileResponse(directory, media_type="octet", filename=db_file.name)


@router.get("/rieslingtest")
async def get_benchmark_file(
    upload_folder=Depends(get_upload_folder),
):
    """A route for obtaining the benchmarking blender file"""
    return FileResponse(
        os.path.join(upload_folder, "rieslingtest.blend"),
        media_type="octet",
        filename="rieslingtest.blend",
    )


@router.get("/smol")
async def get_small_file(
    upload_folder=Depends(get_upload_folder),
):
    """A route for obtaining the much smaller test file"""
    return FileResponse(
        os.path.join(upload_folder, "smol.blend"),
        media_type="octet",
        filename="smol.blend",
    )
