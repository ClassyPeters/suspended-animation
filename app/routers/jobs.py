"""All router functions for interacting with job objects"""
import os

from fastapi import APIRouter, BackgroundTasks, Depends, HTTPException
from sqlalchemy.orm import Session
from starlette.responses import FileResponse

from ..utils import auth, crud, files, schemas
from ..utils.database import get_db
from ..utils.files import get_upload_folder
from ..utils.other import check_for_404

router = APIRouter(prefix="/job", tags=["Jobs"])
allowed_job_statuses = {"started", "canceled", "paused"}
allowed_render_engines = {"BLENDER_EEVEE", "BLENDER_WORKBENCH", "CYCLES"}
allowed_output_file_types = {"archive","mp4"}


@router.post("/render", response_model=schemas.Job)
def create_job(
    job: schemas.JobBase,
    background_tasks: BackgroundTasks,
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    token: str = Depends(auth.oauth2_scheme),
    upload_folder=Depends(get_upload_folder),
):
    """The main function that started it all, this is where a user will create new render jobs"""
    user = auth.get_current_client(db, "user", token)
    if job.render_engine not in allowed_render_engines:
        raise HTTPException(
            status_code=422,
            detail="Render engine not supported, or not entered correctly (Use ALL_CAPS)",
        )
    if job.output_file_type not in allowed_output_file_types:
        raise HTTPException(
                status_code=422,
                detail="Not allowed output file type. Please choose from 'archive' or 'mp4'.",
            )
    db_file = crud.get_file_entry(db, job.file_id)
    if db_file is None or db_file.user != user.id:
        raise HTTPException(status_code=403, detail="invalid file id")
    if db_file.deleted is True:
        raise HTTPException(status_code=403, detail="file has been deleted")
    # enforce useful job names
    # pylint: disable=simplifiable-condition
    if job.name == "string" or None:
        job.name = db_file.name
    db_job = crud.create_job(db, job, user.id, "started")
    if files.check_directory(user, upload_folder) is False:
        files.create_user_directory(user, upload_folder)
    # later on this could be bucket operation with ceph or S3
    files.create_job_directory(user.id, db_job.id, upload_folder)
    # begin task creation and assignment
    background_tasks.add_task(crud.create_render_tasks, db, db_job)
    return db_job


@router.get("/render")
def get_jobs(
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    token: str = Depends(auth.oauth2_scheme),
    status: str = None,
    skip: int = 0,
    limit: int = 100,
):
    """This function returns a list of all jobs created by a user"""
    user = auth.get_current_client(db, "user", token)
    return crud.get_jobs_by_user(db, user.id, status, skip, limit)


@router.get("/render/{job_id}")
def get_job_by_id(
    job_id: int,
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    token: str = Depends(auth.oauth2_scheme),
):
    """The route for users to check on the status of a job"""
    user = auth.get_current_client(db, "user", token)
    job = crud.get_job_by_id(db, job_id)
    check_for_404(job)
    if job.user != user.id:
        return "error"
    return job


@router.put("/render/{job_id}")
def update_job(
    job_id: int,
    background_tasks: BackgroundTasks,
    job_update: schemas.JobUpdate,
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    token: str = Depends(auth.oauth2_scheme),
):
    # pylint: disable=too-many-branches
    """This is for updating jobs already submitted,
    to increase their frame count, pause/resume/cancel"""
    allowed_statuses = {"canceled", "paused", "started", None}
    user = auth.get_current_client(db, "user", token)
    db_job = crud.get_job_by_id(db, job_id)
    check_for_404(db_job)
    if db_job.user != user.id:
        raise HTTPException(status_code=403, detail="No Access to Resource")
    if job_update.status not in allowed_statuses:
        raise HTTPException(status_code=422, detail="Invalid Status")
    if db_job.status == "canceled":
        raise HTTPException(
            status_code=400,
            detail="Job is canceled, cannot start. Please submit a new job.",
        )
    # only update frame count or job status on each request
    if job_update.status is not None:
        if job_update.status == "canceled":
            background_tasks.add_task(crud.cancel_render_tasks, db, db_job.id)
            crud.update_job_status(db, db_job.id, job_update.status)
            db.refresh(db_job)
            return db_job
        if job_update.status == "paused":
            crud.update_job_status(db, db_job.id, job_update.status)
            background_tasks.add_task(crud.pause_render_tasks, db, db_job.id)
            db.refresh(db_job)
        if job_update.status == "started":
            crud.update_job_status(db, db_job.id, job_update.status)
            background_tasks.add_task(crud.resume_render_tasks, db, db_job.id)
            db.refresh(db_job)
    elif job_update.frame_start or job_update.frame_end is not None:
        if job_update.frame_start is not None:
            if job_update.frame_start < db_job.frame_start:
                background_tasks.add_task(
                    crud.increase_job_start, db, db_job.id, job_update
                )
            elif job_update.frame_start > db_job.frame_start:
                background_tasks.add_task(
                    crud.decrease_job_start, db, db_job.id, job_update
                )
        if job_update.frame_end is not None:
            if job_update.frame_end > db_job.frame_end:
                print("increasing task end")
                background_tasks.add_task(
                    crud.increase_job_end, db, db_job.id, job_update
                )
            elif job_update.frame_end < db_job.frame_end:
                print("decreasing task end")
                background_tasks.add_task(
                    crud.decrease_job_end, db, db_job.id, job_update
                )
        db_job.frame_start = job_update.frame_start
        db_job.frame_end = job_update.frame_end
        db.commit()
    db.refresh(db_job)
    return db_job

@router.get("/render/{job_id}/tasks/")
def get_tasks_for_job(
        # pylint: disable=too-many-arguments
    job_id: int,
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    token: str = Depends(auth.oauth2_scheme),
    task_status: str = None,
    skip: int = 0,
    limit: int = 100,
):
    """returns a list of tasks for a specific job"""
    user = auth.get_current_client(db, "user", token)
    db_job = crud.get_job_by_id(db, job_id)
    check_for_404(db_job)
    if db_job.user != user.id:
        raise HTTPException(status_code=403, detail="no access to requested resource")
    return crud.get_tasks_by_job_id(db, job_id, task_status, skip, limit)


@router.get("/render/{job_id}/result/")
async def retrieve_job_result(
    job_id: int,
    token: str = Depends(auth.oauth2_scheme),
    upload_folder=Depends(get_upload_folder),
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
):
    """Route for users to download the final results of their jobs running"""
    user = auth.get_current_client(db, "user", token)
    db_job = crud.get_job_by_id(db, job_id)
    check_for_404(db_job)
    directory_string = ""
    if user.id != db_job.user:
        raise HTTPException(status_code=401, detail="No Access to Requested Resource")
    if db_job.output_file_type == "archive":
        if files.check_for_compressed_file(db_job, upload_folder) is False:
            raise HTTPException(
                status_code=404,
                detail="job not finished yet, or results not compressed yet",
            )
        directory_string = files.get_compressed_file_directory_string(
            db_job, upload_folder
        )
    elif db_job.output_file_type == "mp4":
        if files.check_for_encoded_file(db_job, upload_folder) is False:
            raise HTTPException(
                status_code=404,
                detail="job not finished yet, or results not encoded yet",
            )
        directory_string = files.get_encoded_file_directory_string(
            db_job, upload_folder
        )
    return FileResponse(
        directory_string,
        media_type="octet",
        filename=files.create_output_file_name(db_job),
    )


@router.delete("/render/{job_id}/result/")
async def delete_job_results(
    job_id: int,
    # confirmation_code: str = None,
    background_tasks: BackgroundTasks,
    token: str = Depends(auth.oauth2_scheme),
    upload_folder=Depends(get_upload_folder),
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
):
    """Route for users to delete a job from the server after it's completed"""
    user = auth.get_current_client(db, "user", token)
    db_job = crud.get_job_by_id(db, job_id)
    check_for_404(db_job)
    if user.id != db_job.user:
        raise HTTPException(status_code=401, detail="No Access to Requested Resource")
    background_tasks.add_task(files.delete_job_result, upload_folder, db_job)
    background_tasks.add_task(files.delete_task_results, db, upload_folder, db_job)
    return {"message": "job result deleted"}


@router.get("/render/{job_id}/result/{task_id}")
def get_task_result(
    job_id: int,
    task_id: int,
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    token: str = Depends(auth.oauth2_scheme),
    upload_folder=Depends(get_upload_folder),
):
    """This route returns the resulting image file from a specific task"""
    user = auth.get_current_client(db, "user", token)
    job = crud.get_job_by_id(db, job_id)
    check_for_404(job)
    if job.user != user.id:
        return "error"
    result = crud.get_result_by_task(db, task_id)
    check_for_404(result)
    if job.id != result.job:
        return "error"
    directory = os.path.join(
        upload_folder, str(job.user), str(job.id), str(result.name)
    )
    return FileResponse(directory, media_type="octet", filename=result.name)
