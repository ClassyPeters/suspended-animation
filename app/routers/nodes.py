"""Routes for interacting with worker nodes"""
import os
from datetime import datetime, timedelta
from typing import List

from fastapi import (
    APIRouter,
    BackgroundTasks,
    Depends,
    File,
    HTTPException,
    Query,
    UploadFile,
)
from sqlalchemy.orm import Session
from starlette.responses import FileResponse
from werkzeug.utils import secure_filename

from ..utils import auth, crud, files, schemas
from ..utils.database import get_db
from ..utils.files import get_upload_folder
from ..utils.other import check_for_404

router = APIRouter(prefix="/node", tags=["Worker Nodes"])

allowed_node_statuses = {"online", "created", "maintenance", "expired"}
allowed_task_statuses = {"created", "started", "complete", "canceled"}


def check_maintenance(db_node):
    """Helper function to return an error if a node is undergoing maintenance"""
    if db_node.status == "maintenance":
        raise HTTPException(status_code=400, detail="worker in maintenance mode")

@router.get("/")
def get_all_nodes(
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    token: str = Depends(auth.oauth2_scheme),
    status: str = None,
    skip: int = 0,
    limit: int = 100,
):
    """Return a list of all nodes registered with the server"""
    auth.get_current_client(db, "user", token)
    return crud.get_nodes(db, status, skip, limit)


@router.post("/")
def create_node(
    node: schemas.NodeCreate,
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    token: str = Depends(auth.oauth2_scheme),
):
    """Route for administrators to create new nodes with the server.
    Nodes won't have tasks assigned to them until they call the API and request them"""
    auth.get_current_client(db, "user", token)
    if crud.get_node_by_name(db, node.name) is not None:
        raise HTTPException(
            status_code=400, detail="Nodes must have unique name. Name already taken"
        )
    if node.status not in allowed_node_statuses:
        raise HTTPException(status_code=400, detail="Status not allowed")
    expiration = datetime.utcnow() + timedelta(minutes=30)
    db_node = crud.create_node(db, node, expiration)
    access_token_expires = timedelta(minutes=auth.ACCESS_TOKEN_EXPIRE_MINUTES)
    token = auth.create_access_token(
        data={"token_type": "node", "sub": node.name},
        expires_delta=access_token_expires,
    )
    return [db_node, token]


@router.get("/{node_id}", response_model=schemas.Node)
def get_node(
    node_id: int,
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    token: str = Depends(auth.oauth2_scheme),
):
    """Returns a single node by its ID"""
    auth.get_current_client(db, "user", token)
    db_node = crud.get_node_by_id(db, node_id)
    check_for_404(db_node)
    return db_node


@router.put("/{node_id}", response_model=schemas.Node)
def update_node(
    background_tasks: BackgroundTasks,
    node_id: int,
    node: schemas.NodeCreate,
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    token: str = Depends(auth.oauth2_scheme),
):
    """Route for administrators to update a node's status"""
    auth.get_current_client(db, "user", token)
    if node.status not in allowed_node_statuses:
        raise HTTPException(status_code=400, detail="Status not allowed")
    db_node = crud.update_node(db, node_id, node)
    check_for_404(db_node)
    if node.status == "maintenance":
        background_tasks.add_task(crud.deallocate_all_node_tasks, db, node_id)
    return db_node


@router.get("/job/")
def get_jobs(
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    token: str = Depends(auth.oauth2_scheme),
    status: str = None,
    skip: int = 0,
    limit: int = 100,
):
    """Returns a list of jobs that can be used to determine what files to download"""
    node = auth.get_current_client(db, "node", token)
    check_maintenance(node)
    jobs = crud.get_jobs(db, status, skip, limit)
    return jobs


@router.get("/files/{file_id}")
def get_job_file(
    file_id: int,
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    token: str = Depends(auth.oauth2_scheme),
    upload_folder=Depends(get_upload_folder),
):
    """Route for nodes to download a file for rendering"""
    node = auth.get_current_client(db, "node", token)
    check_maintenance(node)
    file = crud.get_file_entry(db, file_id)
    check_for_404(file)
    directory = files.get_directory_string(
        str(file.user), str(file.id) + ".blend", upload_folder
    )
    return FileResponse(directory, media_type="octet")


@router.get("/token/")
def get_node_token(
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    token: str = Depends(auth.oauth2_scheme),
):
    """This route is for nodes to obtain new tokens"""
    node = auth.get_current_client(db, "node", token)
    check_maintenance(node)
    access_token_expires = timedelta(minutes=auth.ACCESS_TOKEN_EXPIRE_MINUTES)
    token = auth.create_access_token(
        data={"token_type": "node", "sub": node.name},
        expires_delta=access_token_expires,
    )
    return {"token": token}


@router.get("/task/")
def get_node_task(
    file: List[int] = Query(None),
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    token: str = Depends(auth.oauth2_scheme),
    status: str = None,
):
    """Route for Nodes to request a new task based on a list of files they've downloaded"""
    node = auth.get_current_client(db, "node", token)
    check_maintenance(node)
    if file is None:
        raise HTTPException(
            status_code=400, detail="No tasks available due to no files in request"
        )
    # prioritize encoding tasks over render tasks
    # only get one task per call
    task_type = None
    if node.ffmpeg:
        db_task = crud.get_first_incomplete_encode_task(db)
        task_type = "encode"
        if db_task is None and node.render:
            db_task = crud.get_oldest_incomplete_task_by_files(db, file, status)
            task_type = "render"
            check_for_404(db_task)
    elif node.render:
        db_task = crud.get_oldest_incomplete_task_by_files(db, file, status)
        check_for_404(db_task)
        task_type = "render"
    db_task.node = int(node.id)
    db_task.status = "started"
    db_task.start_time = datetime.utcnow()
    db.commit()
    db.refresh(db_task)
    return {"task_type": task_type, "task": db_task}


@router.put("/task/{task_id}")
def update_task(
    # pylint: disable=too-many-arguments
    background_tasks: BackgroundTasks,
    task_id: int,
    task: schemas.TaskUpdate,
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    token: str = Depends(auth.oauth2_scheme),
    upload_folder=Depends(get_upload_folder),
):
    """Route for nodes to update the status of a task"""
    node = auth.get_current_client(db, "node", token)
    check_maintenance(node)
    if task.status not in allowed_task_statuses:
        raise HTTPException(status_code=400, detail="Status not allowed")
    db_task = crud.get_render_task_by_id(db, task_id)
    check_for_404(db_task)
    if task.status == "complete":
        # check that a file was actually uploaded
        db_result = crud.get_result_by_task(db, db_task.id)
        if db_result is None:
            # make the task available for another node to pick up
            db_task.node = None
            db_task.start_time = None
            db_task.status = "created"
            db.commit()
            raise HTTPException(
                status_code=400, detail="No result uploaded, task re-allocated"
            )
        db_task.end_time = datetime.utcnow()
        db_task.status = task.status
        db.commit()
        job_check = crud.get_first_incomplete_task_by_job(db, db_task.job)
        if job_check is None:
            db_job = crud.get_job_by_id(db, db_task.job)
            db_job.end_time = db_task.end_time
            if db_job.output_file_type == "archive":
                db_job.status = "archiving"
                background_tasks.add_task(
                    files.compress_finished_job, db, db_job, upload_folder
                )
            if db_job.output_file_type == "mp4":
                db_job.status = "encoding"
                background_tasks.add_task(crud.create_encode_task, db, db_job)
    db.commit()
    return db_task


@router.post("/task/render/{task_id}/log")
def submit_render_log(
    task_id: int,
    task_log: schemas.TaskLogBase,
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    token: str = Depends(auth.oauth2_scheme),
):
    """Route for nodes to upload logs from a render task"""
    node = auth.get_current_client(db, "node", token)
    db_task = crud.get_render_task_by_id(db, task_id)
    check_for_404(db_task)
    log = task_log.log
    submit_time = datetime.utcnow()
    crud.create_render_task_log(db, task_id, log, node.id, submit_time)
    return {"detail": "success"}


@router.post("/task/encode/{task_id}/log")
def submit_encode_log(
    task_id: int,
    task_log: schemas.TaskLogBase,
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    token: str = Depends(auth.oauth2_scheme),
):
    """Route for nodes to upload logs from an encode task"""
    node = auth.get_current_client(db, "node", token)
    db_task = crud.get_encode_task_by_id(db, task_id)
    check_for_404(db_task)
    log = (task_log.log,)
    submit_time = datetime.utcnow()
    crud.create_encode_task_log(db, task_id, log, node.id, submit_time)
    return {"detail": "success"}


@router.post("/result/{task_type}/{task_id}")
def upload_result(
    # pylint: disable=too-many-arguments
    task_type: str,
    task_id: int,
    file: UploadFile = File(...),
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    token: str = Depends(auth.oauth2_scheme),
    upload_folder=Depends(get_upload_folder),
):
    """Route for nodes to upload the results of a task"""
    node = auth.get_current_client(db, "node", token)
    check_maintenance(node)
    allowed_task_types = ["render", "encode"]
    if task_type not in allowed_task_types:
        raise HTTPException(status_code=404, detail="task type not found")
    if task_type == "render":
        db_task = crud.get_render_task_by_id(db, task_id)
        file.filename = (
            "frame" + str(db_task.frame) + "." + str(db_task.image_format).lower()
        )
    if task_type == "encode":
        db_task = crud.get_encode_task_by_id(db, task_id)
        file.filename = secure_filename(file.filename)
    check_for_404(db_task)
    if node.id != db_task.node:
        raise HTTPException(status_code=403, detail="no access to resouce")
    db_job = crud.get_job_by_id(db, db_task.job)
    # file.filename = secure_filename(file.filename)
    directory = os.path.join(
        upload_folder, str(db_job.user), str(db_job.id), file.filename
    )
    files.save_file(directory, file.file)
    if task_type == "render":
        result = crud.create_result(db, db_job.id, db_task.id, file.filename)
    if task_type == "encode":
        db_job.status = "complete"
    db.commit()
    return result
