"""Routes for administrative tasks"""
from datetime import datetime

from fastapi import APIRouter, BackgroundTasks, Depends, HTTPException
from sqlalchemy.orm import Session

from ..utils import auth, crud
from ..utils.database import get_db
from ..utils.other import check_for_404

router = APIRouter(prefix="/admin", tags=["Administration"])


@router.post("/expiration")
def run_expiration_routine(
    background_tasks: BackgroundTasks,
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    token: str = Depends(auth.oauth2_scheme),
):
    """this route can be used to have the server check for expired nodes, and then de-allocate
    tasks assigned to those expired nodes to be picked up by others"""
    user = auth.get_current_client(db, "user", token)
    if not user.admin:
        raise HTTPException(status_code=401, detail="not authorized")
    expired_nodes = crud.get_expired_nodes(db, datetime.utcnow().timestamp())
    for node in expired_nodes:
        background_tasks.add_task(crud.deallocate_all_node_tasks, db, node.id)
        node.status = "expired"
    db.commit()
    return crud.get_expired_nodes(db, datetime.utcnow().timestamp())


@router.get("/logs/{task_id}")
def get_task_log(
    task_id: int,
    # pylint: disable=invalid-name
    db: Session = Depends(get_db),
    token: str = Depends(auth.oauth2_scheme),
    node_id: int = None,
):
    """this route returns the logs from blender running on each node,
    and can give valuable insight into errors"""
    user = auth.get_current_client(db, "user", token)
    if not user.admin:
        raise HTTPException(status_code=401, detail="not authorized")
    log = crud.get_render_task_log(db, task_id, node_id)
    check_for_404(log)
    return log
