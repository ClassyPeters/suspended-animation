"""Main fastAPI file that builds the application for uvicorn to run"""
import os

from fastapi import FastAPI, Request
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from starlette.responses import FileResponse

from app.routers import admin, auth_router, files, jobs, nodes
from app.utils import settings, models
app = FastAPI()
app.include_router(auth_router.router)
app.include_router(files.router)
app.include_router(jobs.router)
app.include_router(nodes.router)
app.include_router(admin.router)


templates = Jinja2Templates(directory="app/templates")
app.mount("/static", StaticFiles(directory="app/static"), name="static")


@app.on_event("startup")
def delete_test_database():
    """Checks that any unused databases from test startup are deleted"""
    test_db = os.path.join(os.getcwd() + "/test.db")
    if os.path.isfile(test_db):
        os.remove(test_db)
        print("removed test database")
    else:
        pass

@app.on_event("startup")
def create_database():
    if os.environ["SQLALCHEMY_DATABASE_URL"]:
        from app.utils.database import engine
        models.Base.metadata.create_all(bind=engine)
        print("created production database")


@app.get("/")
def index(request: Request):
    """returns the front end built in javascript"""
    return templates.TemplateResponse("index.html", {"request": request})


@app.get("/license")
def return_license():
    """This returns a copy of the license that the software is released under"""
    # maybe find a way of speeding this up
    return FileResponse("LICENSE")

@app.get("/status")
def return_status():
    """This route will be used in future version to inform users
     if a server is undergoing maintenance"""
    return {"status": "online"}
