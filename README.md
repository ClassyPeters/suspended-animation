# Suspended Animation Render Farm

This is a REST API server for managing blender renders. It's built in Python with the incredible FastAPI library, with SQLalchemy used for database interaction. 

### Render Manager used in production at [Celestial Horizons](https://celestialhorizons.io)

### Notable Features:
- Integrated Authentication using signed JWTs 
- Designed for deployment using Docker
- Built for local or widely distributed rendering (use hardware on a locked down LAN, the cloud, or a datacenter) 
- Unified API for job creation/updates, worker node management, and file management
- Extremely minimal external dependencies (only requires an SQL database compatible with [SQL Alchemy](https://docs.sqlalchemy.org/en/13/dialects/))
- Support for multiple render engines
- FFMPEG integration
- Corresponding Worker client [here](https://gitlab.com/ClassyPeters/suspended-animation-worker)
- Efficient (the API can run on a raspberry pi or small virtual machine, so long as there's enough storage space for the Blender files)
- [AGPL Licensed](https://www.gnu.org/licenses/agpl-3.0.html)

### Upcoming Features
- Front end built in React
- Support for object based storage backend (Ceph, Openstack Swift, S3)
- Option for shared NFS volume between worker nodes and API server
- Blender file verification on upload (such as checking for packed textures) 
- Docker Swarm service file for even easier clustered deployments
- Priority based render scheduling (currently first come-first served)

Important note: this application isn't completely safe for the wider internet. Currently there isn't quota based job limiting, bruteforce protection, or password security limitations. In its current state please use it carefully.
It does however have administrative roles, restricted access to files, and only allows administrative users to create new users. 

#### Running In a Virtual Environment (typically for development)
Start by making a directory called venv inside of the cloned repository with "mkdir venv"

Then make a virtual environment with `$ python3 -m venv path/to/venv`

*Ubuntu may require that you install venv from system packages with `$ sudo apt install python3-venv`*

Then start the virtual environment with `$ source venv/bin/activate`
Once activated, type "$ pip install -r requirements.txt" still inside of the root directory where the requirements file is.

Start the server by running `$ uvicorn app.main:app` *there's an optional `--reload` tag to hotload code changes during development*

#### Using Docker
*Do what the cool kids do: [Kubernetes](https://kubernetes.io/), [Docker Swarm](https://docs.docker.com/engine/swarm/), [Openstack Zun](https://docs.openstack.org/zun/latest/), [Podman](https://podman.io/)*

Make sure you have [docker installed](https://docs.docker.com/) and clone this git repository.

Inside of the cloned directory run `$ docker build --tag=suspended-animation .` *that `.` is important and targets the current directory's Dockerfile*

After the container image is done being built, this command starts the server: `$ docker run -p 80:80 suspended-animation`

At that point the API is ready to be consumed. You can visit http://server.address/docs will take you to automatically built documentation that acts as an API client using [swagger](https://swagger.io/solutions/api-documentation/).

*Because of the current experimental status of the React front end it is not currently included in this project's containers*

Pre-Built Docker images will be available once the application is more ready for widespread usage

#### .env file configuration
This project uses an environment file and python's dotenv package to read sensitive information. Currently there are two fields in the file, and there's an empty sample included in the repository:

> SQLALCHEMY_DATABASE_URI=The URI used to connect to a sqlalchemy compatible database. For sqlite this would be `sqlite:///database.db`

> SECRET_KEY=This is the long string used to sign the json web tokens the server returns for authentication. This absolutely needs to stay secret. Make sure you're using a cryptographically secure random generator for this string.

#### Frontend build instructions
Contained in the `app/client` directory is the beginnings for a frontend built in React. It isn't feature complete but is under active development. To build the frontend start by navigating to the `app/client` directory and then run `$ npm install`. That command looks at the `package.json` file and installs the necessary npm packages.

At that point you can run `$ npm start` to run React's development server. In the `package.json` file it's configured to proxy requests through to a locally running API server.

To minify the React code and get it ready for production you can run `$ npm run build` followed by the included bash script `deploy_react.sh` that copies the minified contents into the static and template folders used by the API service.
